import { AfterContentChecked, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment-button',
  templateUrl: './comment-button.component.html',
  styleUrls: ['./comment-button.component.scss']
})
export class CommentButtonComponent implements OnInit, AfterContentChecked {
  @Input() commentNumber = 0;
  @Input() showComments = false;

  public commentIcon = 'chat_bubble_outline';

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterContentChecked(): void {
    this.showComments
    ? this.commentIcon = 'chat_bubble'
    : this.commentIcon = 'chat_bubble_outline';
  }

}
