import { Component, AfterViewInit } from '@angular/core';
import { CardService } from '../../card.service';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements AfterViewInit {
  searchInput = '';

  constructor(public cardService: CardService) { }

  ngAfterViewInit(): void {
  }

  handleInput(): void {
    this.cardService.searchRule = this.searchInput;
  }
}
