import { Pipe, PipeTransform } from '@angular/core';
import { ICardData } from './interfaces';

@Pipe({
  name: 'sort',
  pure: false
})
export class SortPipe implements PipeTransform {
  transform(cardsData: ICardData[], sortRule: string): ICardData[] {
    const data = [...cardsData];
    
    switch (sortRule) {
      case 'date': 
        return data.sort((a, b) => b.date.getTime() - a.date.getTime())
      default:
        return cardsData;
    }
  }

}
