export interface IColumnData {
  id: string;
  columnTitle: string;
}

export interface IComment {
  id: string;
  cardId: string;
  content: string;
  date: Date;
}

export interface ICardData {
  columnId: string;
  cardId: string;
  content: string;
  date: Date;
  likes: number;
  comments: IComment[];
}
