import { Injectable } from '@angular/core';
import { IColumnData, ICardData, IComment } from './interfaces';

@Injectable({ providedIn: 'root' })
export class CardService {
  public sortRule = '';
  public searchRule = '';

  public columnData: IColumnData[] = [
    { id: 'col-1', columnTitle: 'First' },
    { id: 'col-2', columnTitle: 'Second column' },
    { id: 'col-3', columnTitle: 'Third' },
    { id: 'col-4', columnTitle: 'Fourth' },
  ];

  public cardData: ICardData[] = [
    {
      columnId: 'col-1',
      cardId: 'card-1',
      content: 'Add comments',
      date: new Date(2021, 2, 23),
      likes: 0,
      comments: [],
    },
    {
      columnId: 'col-2',
      cardId: 'card-2',
      content: 'Save to local storage',
      date: new Date(),
      likes: 0,
      comments: [
        {
          id: 'com-1',
          cardId: 'card-2',
          content: 'Cool!',
          date: new Date(2021, 2, 29, 18),
        },
      ],
    },
    {
      columnId: 'col-1',
      cardId: 'card-3',
      content: 'Add colors',
      date: new Date(),
      likes: 0,
      comments: [],
    },
    {
      columnId: 'col-2',
      cardId: 'card-4',
      content: 'Improve panel design when board is scrollable',
      date: new Date(2021, 2, 21),
      likes: 0,
      comments: [],
    },
    {
      columnId: 'col-3',
      cardId: 'card-5',
      content: 'Improve design of edit title input and card buttons',
      date: new Date(),
      likes: 0,
      comments: [],
    },
    {
      columnId: 'col-2',
      cardId: 'card-6',
      content: 'Remove margin and padding that make page scrollable',
      date: new Date(),
      likes: 0,
      comments: [],
    },
  ];

  addCard(data: ICardData): void {
    this.cardData.push(data);
  }

  deleteCard(id: string): void {
    this.cardData = this.cardData.filter((card) => card.cardId !== id);
  }

  updateCard(id: string, content: string): void {
    const cardIndex: number = this.cardData.findIndex(
      (card) => card.cardId === id
    );

    if (cardIndex !== -1) {
      this.cardData[cardIndex].content = content;
    }
  }

  addColumn(data: IColumnData): void {
    this.columnData.push(data);
  }

  deleteColumn(id: string): void {
    this.columnData = this.columnData.filter((column) => column.id !== id);
  }

  updateColumn(data: IColumnData): void {
    const { id, columnTitle } = data;
    const columnIndex: number = this.columnData.findIndex(
      (column) => column.id === id
    );

    if (columnIndex !== -1) {
      this.columnData[columnIndex].columnTitle = columnTitle;
    }
  }

  addLike(cardId: string): void {
    const cardIndex: number = this.cardData.findIndex(
      (card) => card.cardId === cardId
    );

    if (cardIndex !== -1) {
      this.cardData[cardIndex].likes = ++this.cardData[cardIndex].likes;
    }
  }

  handleCommentDelete(id: string, cardId: string): void {
    const cardIndex = this.cardData.findIndex((card) => card.cardId === cardId);

    if (cardIndex !== -1) {
      this.cardData[cardIndex].comments = this.cardData[
        cardIndex
      ].comments.filter((comment) => comment.id !== id);
    }
  }

  addComment(newComment: IComment, cardId: string): void {
    const cardIndex = this.cardData.findIndex((card) => card.cardId === cardId);

    this.cardData[cardIndex].comments.push(newComment);
  }
}
