import { Pipe, PipeTransform } from '@angular/core';
import { ICardData } from './interfaces';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(cardData: ICardData[], searchRule: string = ''): ICardData[] {
    if (!searchRule.trim()) {
      return cardData;
    }

    return cardData.filter(card => {
      return card.content.toLowerCase().indexOf(searchRule.toLowerCase()) !== -1;
    });
  }

}
