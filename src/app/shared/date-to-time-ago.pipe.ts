import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateToTimeAgo'
})
export class DateToTimeAgoPipe implements PipeTransform {

  transform(date: Date = new Date()): string {
    const timestamp = date.getTime();
    const currentTime = Date.now();

    const timeDiff = currentTime - timestamp;
    console.log(timeDiff);
    const second = 1000;
    const minute = 60000;
    const hour = minute * 60;
    const day = hour * 12;
    const year = day * 365;

    const getFormatedTime = (diff: number, timeUnit: number) => {
      return Math.floor(diff / timeUnit);
    };

    if (timeDiff < second * 2) {
      return `Now`;
    }

    if (timeDiff < minute) {
      return `${getFormatedTime(timeDiff, second)} second(s) ago`;
    }

    if (timeDiff < hour) {
      return `${getFormatedTime(timeDiff, minute)} minute(s) ago`;
    }

    if (timeDiff < day) {
      return `${getFormatedTime(timeDiff, hour)} hour(s) ago`;
    }

    if (timeDiff < year) {
      return `${getFormatedTime(timeDiff, day)} day(s) ago`;
    }

    if (timeDiff >= year) {
      return `${getFormatedTime(timeDiff, year)} year(s) ago`;
    }

    return (date || new Date()).toDateString();
  }

}
