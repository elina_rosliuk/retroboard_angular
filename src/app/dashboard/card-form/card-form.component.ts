import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { ICardData } from 'src/app/shared/interfaces';


@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.scss'],
})
export class CardFormComponent implements OnInit {
  content = '';

  @Input() showForm = false;
  @Input() column = '';
  @Input() cardContent = '';

  @Output() changedShowForm = new EventEmitter<boolean | string>();
  @Output() newCardData = new EventEmitter<ICardData>();

  constructor(public cardService: CardService) {}

  ngOnInit(): void {
    if (this.cardContent) {
      this.content = this.cardContent;
    }
  }

  handleCloseForm(event: MouseEvent): void {
    event.preventDefault();
    this.content = this.cardContent;
    this.changedShowForm.emit(false);
  }

  handleSubmit(event: MouseEvent): void {
    event.preventDefault();

    if (!this.content.trim()) {
      this.content = this.cardContent;
      this.changedShowForm.emit(false);
      return;
    }

    const data: ICardData = {
      columnId: this.column || '',
      cardId: `card-${Date.now()}`,
      content: this.content,
      date: new Date(),
      likes: 0,
      comments: [],
    };

    this.newCardData.emit(data);
  }
}
