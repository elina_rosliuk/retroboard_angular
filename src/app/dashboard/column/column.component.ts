import { Component, OnInit } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { ICardData, IColumnData } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss'],
})
export class ColumnComponent implements OnInit {
  public showAddForm: boolean | string = false;
  public showEditForm: boolean | string = false;
  public chosenColumnId = '';
  public styleClass = 'defaultColors';

  constructor(public cardService: CardService) {}

  ngOnInit(): void {}

  handleAddForm(columnId: string): void {
    this.showAddForm = columnId;
  }

  setShowForm(changedShowForm: boolean | string): void {
    this.showAddForm = changedShowForm;
  }

  handleDeleteColumn(id: string): void {
    this.cardService.deleteColumn(id);
  }

  addNewCard(data: ICardData): void {
    this.cardService.addCard(data);

    this.showAddForm = false;
  }

  toggleEditForm(id: string): void {
    this.showEditForm = id;
  }

  setEditForm(changedShowForm: boolean | string): void {
    this.showEditForm = changedShowForm;
  }

  editColumnTitle(columnTitle: string): void {
    const columnData: IColumnData = { id: this.showEditForm as string, columnTitle };
    this.cardService.updateColumn(columnData);
  }
}
