import { Component, Input, OnInit } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { ICardData } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input()
  column!: string;

  public styleClass = 'cardColors';
  public isEditing: boolean | string = false;
  public showComments: boolean | string = false;
  newCardContent = '';

  constructor(public cardService: CardService) {}

  ngOnInit(): void {}

  handleDelete(cardId: string): void {
    this.cardService.deleteCard(cardId);
  }

  handleEdit(cardId: string): void {
    this.isEditing = cardId;
    this.newCardContent =  this.cardService.cardData.find(card => card.cardId === cardId)?.content || '';
  }

  setShowForm(): void {
    this.isEditing = false;
  }

  updateCard(data: ICardData): void {
    const newContent: string = data.content;
    this.cardService.updateCard(this.isEditing as string, newContent);
    this.isEditing = false;
  }

  handleAddLike(cardId: string): void {
    console.log(cardId);
    this.cardService.addLike(cardId);
  }

  setShowComments(cardId: string): void {
    this.showComments
    ? this.showComments = false
    : this.showComments = cardId;
  }
}
