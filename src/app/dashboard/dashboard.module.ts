import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { ColumnComponent } from './column/column.component';
import { CardComponent } from './card/card.component';
import { CardFormComponent } from './card-form/card-form.component';
import { FormsModule } from '@angular/forms';
import { PanelComponent } from './panel/panel.component';
import { CrossButtonComponent } from '../shared/components/cross-button/cross-button.component';
import { ConfirmButtonComponent } from '../shared/components/confirm-button/confirm-button.component';
import { DeleteButtonComponent } from '../shared/components/delete-button/delete-button.component';
import { EditButtonComponent } from '../shared/components/edit-button/edit-button.component';
import { ColumnTitleFormComponent } from './column-title-form/column-title-form.component';
import { SortPipe } from '../shared/sort.pipe';
import { SearchPipe } from '../shared/search.pipe';
import { SearchInputComponent } from '../shared/components/search-input/search-input.component';
import { LikeComponent } from '../shared/components/like/like.component';
import { CommentButtonComponent } from '../shared/components/comment-button/comment-button.component';
import { CommentComponent } from './comment/comment.component';
import { DateToTimeAgoPipe } from '../shared/date-to-time-ago.pipe';
import { CommentFormComponent } from './comment-form/comment-form.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ColumnComponent,
    CardComponent,
    CardFormComponent,
    DeleteButtonComponent,
    PanelComponent,
    CrossButtonComponent,
    ConfirmButtonComponent,
    EditButtonComponent,
    ColumnTitleFormComponent,
    SortPipe,
    SearchPipe,
    SearchInputComponent,
    LikeComponent,
    CommentButtonComponent,
    CommentComponent,
    DateToTimeAgoPipe,
    CommentFormComponent,
  ],
  imports: [CommonModule, FormsModule],
  exports: [DashboardComponent],
  providers: [],
})
export class DashboardModule {}
