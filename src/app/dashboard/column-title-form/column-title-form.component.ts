import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { IColumnData } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-column-title-form',
  templateUrl: './column-title-form.component.html',
  styleUrls: ['./column-title-form.component.scss']
})
export class ColumnTitleFormComponent implements OnInit {
  columnTitle = '';

  @Input() showForm = false;
  @Input() title = '';

  @Output() newColumnTitle = new EventEmitter<string>();
  @Output() changedShowForm = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
    if (this.title) {
      this.columnTitle = this.title;
    }
  }

  handleCancel(event: MouseEvent): void {
    event.preventDefault();
    this.changedShowForm.emit(false);
  }

  handleSubmit(event: MouseEvent): void {
    event.preventDefault();

    if (this.columnTitle.trim()) {
      this.newColumnTitle.emit(this.columnTitle);
    }

    this.columnTitle = '';
    this.changedShowForm.emit(false);
  }
}
