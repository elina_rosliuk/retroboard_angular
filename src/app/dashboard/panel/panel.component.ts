import { Component, OnInit } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { IColumnData } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {
  public showColumnForm = false;

  constructor(private cardService: CardService) { }

  ngOnInit(): void {
  }

  handleAddColumn(): void {
    this.showColumnForm = !this.showColumnForm;
  }

  addNewColumn(columnTitle: string): void {
    const newColumn: IColumnData = { id: `col-${Date.now()}`, columnTitle };
    this.cardService.addColumn(newColumn);
  }

  setShowForm(): void {
    this.showColumnForm = false;
  }

  handleSelect(event: any): void {
    this.cardService.sortRule = (event.target as HTMLOptionElement).value;
  }

}
