import { Component, Input, OnInit } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { IComment } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  @Input() cardId!: string;
  content = '';

  constructor(public cardService: CardService) { }

  ngOnInit(): void {
  }

  handleSendComment(): void {
    if (!this.content.trim()) {
      return;
    }
    
    const data: IComment = {
      id: `com-${Date.now()}`,
      cardId: this.cardId,
      content: this.content,
      date: new Date()
    };

    this.cardService.addComment(data, this.cardId);
    this.content = '';
  }

}
