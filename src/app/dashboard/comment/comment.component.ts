import { Component, Input, OnInit } from '@angular/core';
import { CardService } from 'src/app/shared/card.service';
import { IComment } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() commentsData: IComment[] = [];

  constructor(public cardService: CardService) { }

  ngOnInit(): void {
  }

  handleDeleteComment(id: string, cardId: string): void {
    this.cardService.handleCommentDelete(id, cardId);
  }
}
